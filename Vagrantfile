# ATTENTION: please install necessary hostmanager-plugin using the following command:
# vagrant plugin install vagrant-hostmanager

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.hostname = "mg.dev"
  config.vm.box = "ubuntu/trusty64"

  config.vm.provider "virtualbox" do |v|
      v.memory = 4096
      v.cpus = 4
  end

  config.vm.network "private_network", type: "dhcp"
  
# automatically manage /etc/hosts on hosts and guests
  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true

# Custom IP resolver is used to pull the IP out of the private DHCP'd
# interface eth1 as the hostmanager plugin does not support DHCP'd IPs
  config.hostmanager.ip_resolver = proc do |machine|
    result = ""
    machine.communicate.execute("ifconfig eth1") do |type, data|
      result << data if type == :stdout
    end
    (ip = /inet addr:(\d+\.\d+\.\d+\.\d+)/.match(result)) && ip[1]
  end
  
  config.vm.synced_folder "./", "/vagrant", id: "vagrant-root", nfs: true
  #owner: "www-data", group: "www-data"

  config.vm.provision "shell", inline: <<SCRIPT

echo "installing packages (this may take a while)..."
# set parameters for non-interactive dpkg-reconfigure
debconf-set-selections <<< 'mysql-server mysql-server/root_password password test123'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password test123'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/dbconfig-install boolean true'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/app-password-confirm password'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/admin-pass password test123'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/app-pass password'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2'

# installing packages
apt-get update > /dev/null && apt-get -y install apache2 php5 php5-cli php5-curl php5-gd php5-mcrypt php5-mysql php5-oauth mysql-server phpmyadmin graphicsmagick > /dev/null

echo "linking web-root..."
rm -rf /var/www/html && ln -s /vagrant/wp /var/www/html

echo "configuring php..."
sed -i 's/memory_limit = .*/memory_limit = 1536M/' /etc/php5/apache2/php.ini
sed -i 's/upload_max_filesize = .*/upload_max_filesize = 10M/' /etc/php5/apache2/php.ini
sed -i 's/post_max_size = .*/post_max_size = 10M/' /etc/php5/apache2/php.ini
sed -i 's/max_execution_time = .*/max_execution_time = 240/' /etc/php5/apache2/php.ini

echo "configuring apache..."
sed -e '14i\<Directory /var/www/html>\\n		AllowOverride All\\n	</Directory>\\n' /etc/apache2/sites-available/000-default.conf > /etc/apache2/sites-available/001-vagrant.conf
a2enmod rewrite > /dev/null
a2dissite 000-default > /dev/null && a2ensite 001-vagrant > /dev/null && service apache2 reload > /dev/null

#checkDatabaseCommand=$(echo "show databases;" | mysql -u root -ptest123 | grep -E "^benz_weine" | wc -l)
#if [ $checkDatabaseCommand -eq 0 ]
#  then
#    echo "Importing MySQL Dumpfile..."
#    mysql -u root -ptest123 -h localhost < /vagrant/dbdump.sql
#  else
#    echo "Skipping import of MySQL Dumpfile (database already exists)"
#fi

echo "install compass"

apt-get install -y git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties ruby-dev libgdbm-dev libncurses5-dev automake libtool bison libffi-dev

gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
curl -sS -L https://get.rvm.io | bash -s stable
source ~/.rvm/scripts/rvm
echo "source ~/.rvm/scripts/rvm" >> ~/.bashrc
rvm install 2.1.2
rvm use 2.1.2 --default
ruby -v

gem install compass

echo
echo "Your machine is now available as http://mg.dev/"
echo "phpMyAdmin URL: http://mg.dev/phpmyadmin/"
echo "MySQL user: root"
echo "MySQL password: test123"
SCRIPT
end
